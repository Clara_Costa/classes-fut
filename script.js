
let jogadores = [];
import jogador from "./classes/Jogador.js";
window.Cadastrar = () => {
  let nomeDoJogador = byId("nomeDoJogador").value;
  let numeroDoJogador = byId("numeroDoJogador").value;
  let posicaoDoJogador = byId("posicaoDoJogador").value;
  let jogador1 = new jogador(nomeDoJogador, numeroDoJogador, posicaoDoJogador);
  jogadores.push(jogador1);
  apresentarjogador();
}

function apresentarjogador() {
  let tabela = byId("tabela1");
  let lista = "<ol>"
  for (let jogador1 of jogadores) {
    lista += `<li>${jogador1.nome}, ${jogador1.numero}, ${jogador1.posicao}</li>`;
  }
  lista += "</ol>"
  tabela.innerHTML = lista;
}


let Times = [];
import Time from "./classes/Time.js";


window.Cadastrar1 = function Cadastrar1() {
  let nomeDoTime = byId("nomeDoTime").value;
  let deOndeE = byId("deOndeE").value;
  let time1 = new Time(nomeDoTime, deOndeE);
  Times.push(time1);
  apresentarTime();
}

function apresentarTime() {
  let tabela = byId("tabela4");
  let lista = "<ol>"
  for (let time1 of Times) {
    lista += `<li>${time1.nome}, ${time1.deOndeE}</li>`;
  }
  lista += "</ol>"
  tabela.innerHTML = lista;
}

let arbitros = [];
import Arbitro from "./classes/arbitro.js";

window.Cadastrar2 = function Cadastrar2() {
  let nome = byId("nomeDoArbitro").value;
  let nivel = byId("nivel").value;
  let arbitro1 = new Arbitro(nome, nivel);
  arbitros.push(arbitro1);
  apresentararbitro();
}
function apresentararbitro() {
  let tabela = byId("tabela6");
  let lista = "<ol>"
  for (let arbitro1 of arbitros) {
    lista += `<li>${arbitro1.nome}, ${arbitro1.nivel}</li>`;
  }
  lista += "</ol>"
  tabela.innerHTML = lista;
}

function byId(id) {
  return document.getElementById(id);
}

let estadios = [];
import Estadio from "./classes/estadio.js";

window.Cadastrar3 = function Cadastrar3() {
  let nome = byId("nomeDoEstadio").value;
  let localizacao = byId("localizacao").value;
  let estadio1 = new Estadio(nome, localizacao);
  estadios.push(estadio1);
  apresentarestadio();
}
function apresentarestadio() {
  let tabela = byId("tabela8");
  let lista = "<ol>"
  for (let estadio1 of estadios) {
    lista += `<li>${estadio1.nome}, ${estadio1.localizacao}</li>`;
  }
  lista += "</ol>"
  tabela.innerHTML = lista;
}

